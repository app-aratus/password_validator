#!/bin/csh -f

foreach password ( "abcdefg5" \
                   "abcdefg5!" \
                   "abcdEfgh" \
                   "abc5" \
                   "abcdefghijklmnopqrs9" \
                   "abcdefg5zzz" \
                   "abcdefg5zz" \
                   "12345678" )

    echo '------------------------------------------------------------------------------------------------------'
    echo "java -Djava.awt.headless=true -Xmx20m -jar target/passwordvalidate-0.0.1-SNAPSHOT.jar $password"
          java -Djava.awt.headless=true -Xmx20m -jar target/passwordvalidate-0.0.1-SNAPSHOT.jar $password

end

echo '------------------------------------------------------------------------------------------------------'
