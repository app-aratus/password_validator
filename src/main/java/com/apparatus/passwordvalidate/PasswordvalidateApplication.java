package com.apparatus.passwordvalidate;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.apparatus.passwordvalidate.PasswordConstraintValidator;

@SpringBootApplication
public class PasswordvalidateApplication {

	public static void main(String[] args) {		
		
		/*
		 *  Would be run from the command line like this-
         *  java -Djava.awt.headless=true -Xmx20m -jar target/passwordvalidate-0.0.1-SNAPSHOT.jar abcdefg5
         *  The following is a valid password:      abcdefg5
		 */
		
		String inputStringFromArgs = args[0];

		PasswordConstraintValidator passwordConstraintValidator = new PasswordConstraintValidator();
		boolean validPassword = passwordConstraintValidator.isValid( inputStringFromArgs );
		if ( validPassword == true ) {
			System.out.println( "The following is a valid password:      " + inputStringFromArgs );
		} else {
			System.out.println( "The following is not a valid password:  " + inputStringFromArgs );
		}
	}
}
