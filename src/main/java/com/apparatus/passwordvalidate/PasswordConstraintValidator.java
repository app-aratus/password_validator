package com.apparatus.passwordvalidate;

import java.util.Arrays;
import org.passay.AllowedCharacterRule;
import org.passay.AlphabeticalCharacterRule;
import org.passay.DigitCharacterRule;
import org.passay.LengthRule;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.RuleResult;
import org.passay.RepeatCharacterRegexRule;

public class PasswordConstraintValidator {

    public boolean isValid( String password ) {

    	PasswordValidator validator = new PasswordValidator( Arrays.asList(
     			new AllowedCharacterRule(new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' }),
     			new DigitCharacterRule(1),
     			new AlphabeticalCharacterRule(1),
     			new LengthRule(5, 12),
     			new RepeatCharacterRegexRule(3)
    		) );
    	
        final RuleResult result = validator.validate( new PasswordData(password) );
        
        return result.isValid();
    }
	
}

//new AllowedCharacterRule(new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' }),
//new LowercaseCharacterRule(1),
//new AllowedCharacterRule(new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' }),
//new DigitCharacterRule(1),
//new LengthRule(5, 12),
//new RepeatCharacterRegexRule(3)