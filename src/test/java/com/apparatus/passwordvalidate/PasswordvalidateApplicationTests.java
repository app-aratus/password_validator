package com.apparatus.passwordvalidate;

import com.apparatus.passwordvalidate.PasswordConstraintValidator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PasswordvalidateApplicationTests {

	@Test
	public void contextLoads() {
		
		PasswordConstraintValidator passwordConstraintValidator = new PasswordConstraintValidator();
		
		// Everything right, this should pass
		assertTrue( passwordConstraintValidator.isValid("abcdefg5") );
		
		// Special character, this should fail
		assertFalse( passwordConstraintValidator.isValid("abcdefg5!") );
		
		// All letters with no number, this should fail
		assertFalse( passwordConstraintValidator.isValid("abcdEfgh") );

		// Under minimum size, this should fail
		assertFalse( passwordConstraintValidator.isValid("abc5") );

		// Over size, this should fail
		assertFalse( passwordConstraintValidator.isValid("abcdefghijklmnopqrs9") );

		// Repeating characters, this should fail (we must have 3 repeating characters)
		assertFalse( passwordConstraintValidator.isValid("abcdefg5zzz") );

		// Repeating characters, with only 2 repeating characters, this should pass
		assertTrue( passwordConstraintValidator.isValid("abcdefg5zz") );
		
		// All numbers with no letters, this should fail
		assertFalse( passwordConstraintValidator.isValid("12345678") );

		
		
	}

}
